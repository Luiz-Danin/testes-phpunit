<?php

declare(strict_types=1);
namespace App;

use Closure;

class ArrowsFunctions
{
    public array $arrayNumeros = [];

    public function multiplosDeDez(array $numeros): array
    {
        $this->arrayNumeros =  array_filter($numeros, fn($item) => ($item % 10) === 0 );
        return $this->arrayNumeros;
    }

    public function multiplosDeCinco(array $numeros): array
    {
        $filtrar = fn($item) => ($item % 5) === 0;
        return  array_filter($numeros, $filtrar);
    }

    public function multiplosDinamico(array $numeros, int $multiplo): array
    {
        return array_filter($numeros, fn($item) => ($item % $multiplo) === 0);
    }

    public function retornaApenasNumeros(string $texto): string
    {
        $retorno = fn($texto) => preg_replace('/\D/', '', $texto);
        return $retorno($texto);
    }

    public function retornaApenasLetrasMaiuscula(string $texto): string
    {
        $retorno = fn($texto) => preg_replace('/[^A-Z]/', '', $texto);
        return $retorno($texto);
    }

    public function retornaApenasLetrasMinuscula(string $texto): string
    {
        $retorno = fn($texto) => preg_replace('/[^a-z]/', '', $texto);
        return $retorno($texto);
    }

    public function retornaFiltrosDinamicamente(string $texto, string $tipo): string
    {
        $retorno = match ($tipo) {
            'numeros' => fn($texto) => preg_replace('/\D/', '', $texto),
            'maiusculas' => fn($texto) => preg_replace('/[^A-Z]/', '', $texto),
            'minusculas' => fn($texto) => preg_replace('/[^a-z]/', '', $texto),
        };

        return $retorno($texto);
    }

    public function formatarParaMaiuscula(string $texto): string
    {
        $formatar = fn($texto) => mb_strtoupper($texto);

        return $formatar($texto);
    }

    public function formatarParaMaiusculaRetorno(): Closure
    {
        return fn($texto) => mb_strtoupper($texto);
    }

    public function formatarStringDinamico(string $texto, string $parametroFormatacao): string
    {
        $function = [
            'up' => fn($texto) => mb_strtoupper($texto),
            'down' =>  fn($texto) => mb_strtolower($texto),
            'hide' =>  fn($texto) => str_pad('', strlen($texto), '*'),
        ];
   
        return $function[$parametroFormatacao]($texto);
    }

    public function formatarStringDinamicoComSwitch(string $texto, string $tipo): string
    {
        $result = '';
        switch ($tipo) {
            case 'up':
                $result =  fn($texto) => mb_strtoupper($texto);
                break;
            case 'down':
                $result = fn($texto) => mb_strtolower($texto);
                break;
            case 'hide':
                $result = fn ($texto) => str_pad('', strlen($texto), '*');
                break;
        }
        return $result($texto);
    }
}