<?php

declare(strict_types=1);
namespace App;

use Closure;

class AnonymousFunctions
{
    public array $arrayNumeros = [];

    public function multiplosDeDez(array $numeros): array
    {
        $this->arrayNumeros =  array_filter($numeros, function($item) {
            return ($item % 10) === 0;
        });
       
        return $this->arrayNumeros;
    }

    public function multiplosDeCinco(array $numeros): array
    {
        $filtrar = function($item){
            return ($item % 5) === 0;
        };

        return  array_filter($numeros, $filtrar);
    }

    public function multiplosDinamico(array $numeros, int $multiplo): array
    {
        return array_filter($numeros, function($item) use ($multiplo) {
            return ($item % $multiplo) === 0;
        });
    }

    public function formatarParaMaiuscula(string $texto): void
    {
        $formatar = function($texto) {
            return mb_strtoupper($texto);
        };

        echo $formatar($texto);
    }

    public function formatarParaMaiusculaRetorno(): Closure
    {
        return function($texto) {
            return mb_strtoupper($texto);
        };
    }

    public function formatarStringDinamico(string $texto, string $parametroFormatacao): string
    {
        $function = [
            'up' => function($texto){
                return mb_strtoupper($texto);
            },
            'down' =>  function($texto){
                return mb_strtolower($texto);
            },
            'hide' =>  function($texto){
                return str_pad('', strlen($texto), '*');
            },
        ];
   
        return $function[$parametroFormatacao]($texto);
    }

    public function formatarStringDinamicoComSwitch(string $texto, string $tipo): string
    {
        $result = '';
        switch ($tipo) {
            case 'up':
                $result =  function($texto){
                    return mb_strtoupper($texto);
                };
                break;
            case 'down':
                $result = function($texto){
                    return mb_strtolower($texto);
                };
                break;
            case 'hide':
                $result = function($texto){
                    return str_pad('', strlen($texto), '*');
                };
                break;
        }
        return $result($texto);
    }
}