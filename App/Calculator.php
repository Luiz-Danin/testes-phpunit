<?php

declare(strict_types=1);
namespace App;

class Calculator
{
    public function sum(int|float ... $numbers): float|int
    {
        $sum = 0;
        foreach($numbers as $number) {
            $sum += $number;
        }
        return $sum;
    }

    public function subtraction()
    {
       
    }
}