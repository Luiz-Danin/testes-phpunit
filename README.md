# testes-phpunit


## Executar testes

- [RUN]
```
./vendor/bin/phpunit 
```

## Executar testes com coverage

- [XDEBUG_MODE]
```
export XDEBUG_MODE=coverage 
```

- [Comando]
```
./vendor/bin/phpunit --coverage-html public/reports/coverage --stder 
```
