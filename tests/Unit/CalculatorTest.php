<?php

use PHPUnit\Framework\TestCase;
use App\Calculator;

class CalculatorTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

   
    // public function it_should_do_a_sum_and_return_the_result()
    // {
    //     $sum = new Calculator();
    //     $result = $sum->sum(10,8.5);
    //     $this->assertEquals(18.5, $result);
    // }

    /**
     * @dataProvider numbersDataProvider
     * @test
     */
    public function it_should_do_a_sum_and_return_the_result($number1, $number2, $expected)
    {
        $sum = new Calculator();
        $result = $sum->sum($number1, $number2);
        $this->assertEquals($expected, $result);
    }

    public function numbersDataProvider(): array
    {
        return [
            [10, 20, 30],
            [1.5, 10, 11.5],
            [-10, 20, 10],
            [1, -20, -19],
            [1.8, 1.8, 3.6],
        ];
    }

    /**
     * @return void
     * @test
     */
    /*
    public function it_should_do_a_sum_negative_numbers()
    {
        $sum = new Calculator();
        $result = $sum->sum(-1, -10);
        $this->assertEquals(-11, $result);
    }
    */

    /**
     * @return void
     * @test
     */
    /*public function it_should_do_a_sum_two_float_numbers()
    {
        $sum = new Calculator();
        $result = $sum->sum(10.5,8.5);
        $this->assertEquals(19, $result);
    }
    */

    /**
     * @return void
     * @test
     */

    /*
    public function it_should_do_a_sum_two_integer_numbers()
    {
        $sum = new Calculator();
        $result = $sum->sum(10,8);
        $this->assertEquals(18, $result);
    }
    */
}