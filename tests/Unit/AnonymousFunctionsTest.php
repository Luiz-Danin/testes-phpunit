<?php

use PHPUnit\Framework\TestCase;
use App\AnonymousFunctions;

class AnonymousFunctionsTest extends TestCase
{
    public AnonymousFunctions $anonymousFunctions;

    protected function setUp(): void
    {
        parent::setUp();
        $this->anonymousFunctions = new AnonymousFunctions();
    }

    public function testMultiplosDeDez()
    {
        $arrayNumeros = [10, 13, 15, 20, 27, 29, 35, 40];
        $multiplosDeDez = $this->anonymousFunctions->multiplosDeDez($arrayNumeros);
       
        $this->assertIsArray($multiplosDeDez);
        $this->assertEquals(10, $multiplosDeDez[0]);
        $this->assertEquals(20, $multiplosDeDez[3]);
        $this->assertEquals(40, $multiplosDeDez[7]);
        $this->assertContains(10, $multiplosDeDez);
    }

    public function testMultiplosDeCinco()
    {
        $arrayNumeros = [10, 13, 15, 20, 27, 29, 35, 40];
        $multiplosDeCinco = $this->anonymousFunctions->multiplosDeCinco($arrayNumeros);

        $this->assertIsArray($multiplosDeCinco);
        $this->assertEquals(10, $multiplosDeCinco[0]);
        $this->assertEquals(15, $multiplosDeCinco[2]);
        $this->assertEquals(20, $multiplosDeCinco[3]);
        $this->assertEquals(35, $multiplosDeCinco[6]);
        $this->assertEquals(40, $multiplosDeCinco[7]);
        $this->assertContains(10, $multiplosDeCinco);
    }

    public function testMultiplosDeCincoDinamico()
    {
        $arrayNumeros = [10, 13, 15, 20, 27, 29, 35, 40];
        $multiplosDeCinco = $this->anonymousFunctions->multiplosDeCinco($arrayNumeros);

        $this->assertIsArray($multiplosDeCinco);

        foreach($multiplosDeCinco as $multiplos)
        {
            $this->assertEquals(true, ($multiplos % 5) === 0);
        }
    }

    public function testMultiplosDinamico()
    {
        $arrayNumeros = [10, 13, 15, 20, 27, 29, 35];
        $multiplo = 8;
        $multiplosDeOito = $this->anonymousFunctions->multiplosDinamico($arrayNumeros, $multiplo);
       
        $this->assertIsArray($multiplosDeOito);
        foreach($multiplosDeOito as $multiplos)
        {
            $this->assertEquals(true, ($multiplos % $multiplo) === 0);
        }
    }

    public function testFormatarParaMaiuscula()
    {
        $texto  = 'luiz danin';
       
        $formatarParaMaiuscula = $this->anonymousFunctions->formatarParaMaiuscula($texto);
        $this->assertNull($formatarParaMaiuscula);
    }

    public function testFormatarParaMaiusculaComRetorno()
    {
        $texto  = 'luiz danin';
       
        $formatarParaMaiuscula = $this->anonymousFunctions->formatarParaMaiusculaRetorno();
        $this->assertStringMatchesFormat ( 'LUIZ DANIN' ,  $formatarParaMaiuscula($texto) );
    }

    public function testFormatarStringUP()
    {
        $texto  = 'luiz danin';
        $parametroFormatacao = 'up';
       
        $formatarParaMaiuscula = $this->anonymousFunctions->formatarStringDinamico($texto, $parametroFormatacao);
        $this->assertStringMatchesFormat( 'LUIZ DANIN' ,  $formatarParaMaiuscula );
    }

    public function testFormatarStringDown()
    {
        $texto  = 'LUIZ DANIN';
        $parametroFormatacao = 'down';
       
        $formatarParaMinuscula = $this->anonymousFunctions->formatarStringDinamico($texto, $parametroFormatacao);
        $this->assertStringMatchesFormat( 'luiz danin' ,  $formatarParaMinuscula );
    }

    public function testFormatarStringHide()
    {
        $texto  = 'LUIZ DANIN';
        $parametroFormatacao = 'hide';
       
        $formatarParaHide = $this->anonymousFunctions->formatarStringDinamico($texto, $parametroFormatacao);
        $this->assertStringMatchesFormat( '**********' ,  $formatarParaHide );
    }

    public function testFormatarStringDinamicoUPComSwitch()
    {
        $formatarParaMaiuscula = $this->anonymousFunctions->formatarStringDinamicoComSwitch(texto: 'luiz danin', tipo: 'up');
        $this->assertStringMatchesFormat( 'LUIZ DANIN' ,  $formatarParaMaiuscula );
    }

    public function testFormatarStringDinamicoDownComSwitch()
    {
        $formatarParaMinuscula = $this->anonymousFunctions->formatarStringDinamicoComSwitch(texto: 'LUIZ DANIN', tipo: 'down');
        $this->assertStringMatchesFormat( 'luiz danin' ,  $formatarParaMinuscula );
    }

    public function testFormatarStringDinamicoHideComSwitch()
    {
        $formatarParaHide = $this->anonymousFunctions->formatarStringDinamicoComSwitch(texto: 'LUIZ DANIN', tipo: 'hide');
        $this->assertStringMatchesFormat( '**********' ,  $formatarParaHide );
    }
}