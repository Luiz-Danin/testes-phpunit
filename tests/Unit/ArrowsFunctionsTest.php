<?php

use PHPUnit\Framework\TestCase;
use App\ArrowsFunctions;

class ArrowsFunctionsTest extends TestCase
{
    public ArrowsFunctions $arrowFunctions;

    protected function setUp(): void
    {
        parent::setUp();
        $this->arrowFunctions = new ArrowsFunctions();
    }

    public function testMultiplosDeDez()
    {
        $arrayNumeros = [10, 13, 15, 20, 27, 29, 35, 40];
        $multiplosDeDez = $this->arrowFunctions->multiplosDeDez($arrayNumeros);

        foreach($multiplosDeDez as $multiplos)
        {
            $this->assertEquals(true, ($multiplos % 10) === 0);
        }
    }

    public function testMultiplosDeCinco()
    {
        $arrayNumeros = [10, 13, 15, 20, 27, 29, 35, 40];
        $multiplosDeCinco = $this->arrowFunctions->multiplosDeCinco($arrayNumeros);

        foreach($multiplosDeCinco as $multiplos)
        {
            $this->assertEquals(true, ($multiplos % 5) === 0);
        }
    }

    public function testMultiplosDinamico()
    {
        $arrayNumeros = [8, 10, 13, 15, 20, 27, 29, 35, 64];
        $multiplo = 8;
        $multiplosDeOito = $this->arrowFunctions->multiplosDinamico($arrayNumeros, $multiplo);
       
        $this->assertIsArray($multiplosDeOito);
        foreach($multiplosDeOito as $multiplos)
        {
            $this->assertEquals(true, ($multiplos % $multiplo) === 0);
        }
    }

    public function testRetornaApenasNumeros()
    {
        $retornaApenasNumeros = $this->arrowFunctions->retornaApenasNumeros(texto:'New 2023');
        $this->assertEquals('2023', $retornaApenasNumeros);
    }

    public function testRetornaApenasLetrasMaiuscula()
    {
        $retornaApenasNumeros = $this->arrowFunctions->retornaApenasLetrasMaiuscula(texto:'NEW 2023');
        $this->assertEquals('NEW', $retornaApenasNumeros);
    }

    public function testRetornaApenasLetrasMinuscula()
    {
        $retornaApenasNumeros = $this->arrowFunctions->retornaApenasLetrasMinuscula(texto:'NEW 2023');
        $this->assertEquals('', $retornaApenasNumeros);
    }

    public function testRetornaFiltrosDinamicamentePorNumero()
    {
        $retornaApenasNumeros = $this->arrowFunctions->retornaFiltrosDinamicamente(texto:'NEW 2023', tipo: 'numeros');
        $this->assertEquals('2023', $retornaApenasNumeros);
    }

    public function testRetornaFiltrosDinamicamentePorLetraMaiuscula()
    {
        $retornaApenasNumeros = $this->arrowFunctions->retornaFiltrosDinamicamente(texto:'NEW 2023', tipo: 'maiusculas');
        $this->assertEquals('NEW', $retornaApenasNumeros);
    }

    public function testRetornaFiltrosDinamicamentePorLetraMinuscula()
    {
        $retornaApenasNumeros = $this->arrowFunctions->retornaFiltrosDinamicamente(texto:'NEW 2023', tipo: 'minusculas');
        $this->assertEquals('', $retornaApenasNumeros);
    }

    public function testFormatarParaMaiuscula()
    {
        $formatarParaMaiuscula = $this->arrowFunctions->formatarParaMaiuscula('textos');
        $this->assertEquals('TEXTOS', $formatarParaMaiuscula);
    }

    public function testFormatarParaMaiusculaRetorno()
    {
        $texto = 'text';
        $formatarParaMaiuscula = $this->arrowFunctions->formatarParaMaiusculaRetorno();
        $this->assertEquals('TEXT', $formatarParaMaiuscula($texto) );
    }

    public function testFormatarStringDinamicoUP()
    {
        $texto = 'text';
        $tipo = 'up';
        $formatarStringDinamico = $this->arrowFunctions->formatarStringDinamico(texto: $texto, parametroFormatacao: $tipo);

        $this->assertEquals('TEXT', $formatarStringDinamico );
    }

    public function testFormatarStringDinamicoDown()
    {
        $texto = 'TEX';
        $tipo = 'down';
        $formatarStringDinamico = $this->arrowFunctions->formatarStringDinamico(texto: $texto, parametroFormatacao: $tipo);

        $this->assertEquals('tex', $formatarStringDinamico );
    }

    public function testFormatarStringDinamicoHide()
    {
        $texto = 'TEX';
        $tipo = 'hide';
        $formatarStringDinamico = $this->arrowFunctions->formatarStringDinamico(texto: $texto, parametroFormatacao: $tipo);

        $this->assertEquals('***', $formatarStringDinamico );
    }

    public function testFormatarStringDinamicoComSwitchUP()
    {
        $texto = 'upper';
        $tipo = 'up';
        $formatarStringDinamicoComSwitch = $this->arrowFunctions->formatarStringDinamicoComSwitch(texto: $texto, tipo: $tipo);

        $this->assertEquals('UPPER', $formatarStringDinamicoComSwitch );
    }

    public function testFormatarStringDinamicoComSwitchDown()
    {
        $texto = 'DOWN';
        $tipo = 'down';
        $formatarStringDinamicoComSwitch = $this->arrowFunctions->formatarStringDinamicoComSwitch(texto: $texto, tipo: $tipo);

        $this->assertEquals('down', $formatarStringDinamicoComSwitch );
    }

    public function testFormatarStringDinamicoComSwitchHide()
    {
        $texto = 'hide';
        $tipo = 'hide';
        $formatarStringDinamicoComSwitch = $this->arrowFunctions->formatarStringDinamicoComSwitch(texto: $texto, tipo: $tipo);

        $this->assertEquals('****', $formatarStringDinamicoComSwitch );
    }
}